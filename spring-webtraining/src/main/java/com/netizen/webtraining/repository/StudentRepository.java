package com.netizen.webtraining.repository;

import com.netizen.webtraining.model.StudentEntity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ABsiddik on 2/4/2017.
 */
@Repository
public class StudentRepository {

    @Autowired
    @Qualifier(value = "sessionFactory")
    private SessionFactory sessionFactory;

    public void createStudent(StudentEntity studentEntity) {
       sessionFactory.getCurrentSession().save(studentEntity);
    }

    public List<StudentEntity> getAll() {
        return sessionFactory.getCurrentSession().createQuery("from StudentEntity")
                .list();
    }

    public StudentEntity getStudentById(long id) {
        return this.sessionFactory.getCurrentSession().get(StudentEntity.class, id);
    }

    public void update(StudentEntity studentEntity) {
        sessionFactory.getCurrentSession().update(studentEntity);
    }

    public void delete(StudentEntity studentEntity) {
        sessionFactory.getCurrentSession().delete(studentEntity);
    }
}
