package com.netizen.webtraining.controller;

import com.netizen.webtraining.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by AB SIDDIK on 1/28/2017.
 */
@Controller
@RequestMapping("/")
public class HelloController {

    @Autowired
    private HelloService helloService;

    @GetMapping
    public String index(Model model) {
        model.addAttribute("message", "Hello all how are you?");
        return "hello";
    }

    @RequestMapping(value = "/hello/{id}/{name}", method = RequestMethod.GET)
    public String sayHelloRiaz(Model model, @PathVariable String id, @PathVariable String name) {
        System.out.println("ID : "+id+", Name : "+name);
        model.addAttribute("student", helloService.getStudent(id));
        return "hello";
    }

    @RequestMapping(value = "/admin")
    public String helloAdmin(){
        return "admin/admin-page";
    }

    @RequestMapping(value = "/adminInfo")
    public String getAdmin(@RequestParam(value = "id", defaultValue = "riad") String id,  Model model){
        model.addAttribute("student", helloService.getStudent(id));
        return "admin/admin-page";
    }

    @RequestMapping(value = "test")
    public String test() {
        return "hello";
    }


}
