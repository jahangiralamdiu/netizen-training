package com.netizen.webtraining.controller;

import com.netizen.webtraining.model.Student;
import com.netizen.webtraining.service.HelloService;
import com.netizen.webtraining.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ABsiddik on 2/4/2017.
 */
@RestController
public class HelloFromRest {
    @Autowired
    private StudentService studentService;

    @ResponseBody
    @RequestMapping(value = "/student/{id}")
    public Student getStudentById(@PathVariable  long id) {
        return studentService.getStudentById(id);
    }
}
