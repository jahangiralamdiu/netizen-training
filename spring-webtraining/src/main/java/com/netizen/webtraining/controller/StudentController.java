package com.netizen.webtraining.controller;

import com.netizen.webtraining.model.Student;
import com.netizen.webtraining.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ABsiddik on 2/4/2017.
 */
@Controller
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/student/create", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("studentList", studentService.getAll());
        return "student-create";
    }

    @RequestMapping(value = "/student/create", method = RequestMethod.POST)
    public String create(@ModelAttribute("student") Student student) {
        studentService.saveStudent(student);
        return "redirect:/student/create";
    }
}
