package com.netizen.webtraining.service;

import com.netizen.webtraining.model.Student;
import com.netizen.webtraining.model.StudentEntity;
import com.netizen.webtraining.repository.StudentRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ABsiddik on 2/4/2017.
 */
@Service
@Transactional
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public void saveStudent(Student student) {
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setUserName(student.getUserName());
        studentEntity.setName(student.getName());
        studentEntity.setEmail(student.getEmail());
        studentRepository.createStudent(studentEntity);
    }

    public List<Student> getAll() {
        List<StudentEntity> studentEntities = studentRepository.getAll();
        List<Student> students = new ArrayList<>();
        for(StudentEntity studentEntity : studentEntities) {
            Student student = new Student();
            BeanUtils.copyProperties(studentEntity, student);
            students.add(student);
        }
        return students;
    }

    public Student getStudentById(long id) {
        StudentEntity studentEntity = this.studentRepository.getStudentById(id);
        Student student = null;
        if (studentEntity != null) {
            student = new Student();
            BeanUtils.copyProperties(studentEntity, student);
        }

        return student;
    }

    public void updateStudent(Student student) {
        StudentEntity studentEntity = this.studentRepository.getStudentById(student.getId());
    }
}
