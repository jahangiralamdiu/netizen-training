package com.netizen.webtraining.service;

import com.netizen.webtraining.model.Student;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by AB SIDDIK on 1/28/2017.
 */
@Service
public class HelloService {

    private Map<String, Student> students;

    public HelloService() {
        students = new HashMap<>();
        Student riad = new Student("riad", "Riad Mahamud", "riad@gmail.com");
        Student nasir = new Student("nasir", "Nasir", "nasir@gmail.com");
        students.put("riad", riad);
        students.put("nasir", nasir);
    }

    public Student getStudent(String id) {
        return students.get(id);
    }

}
