package com.netizen.webtraining.model;

/**
 * Created by AB SIDDIK on 1/28/2017.
 */
public class Student {
    private long id;
    private String userName;
    private String name;
    private String email;

    public Student() {
    }

    public Student(String userName, String name, String email) {
        this.setUserName(userName);
        this.name = name;
        this.email = email;
    }

    public Student(long id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
