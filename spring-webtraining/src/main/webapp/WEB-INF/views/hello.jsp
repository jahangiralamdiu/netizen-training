<%--
  Created by IntelliJ IDEA.
  User: AB SIDDIK
  Date: 1/28/2017
  Time: 10:40 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello All</title>
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
</head>
<body>
<h1>ID : <span id="sid"></span></h1>
<h1>Name : <span id="name"></span></h1>
<h1>Email : <span id="email"></span></h1>
    ID : <input id="input-id" type="text" name="id"/>
    <button id="search" value="Search">Search</button>
<script>
    $(document).ready(function () {
        $('#search').on('click', function () {
            searchStudent($('input[name="id"]').val());
        });
        function searchStudent(studentId) {
            $.get("http://localhost:8080/student/"+studentId, function(data ) {
                if (data) {
                    $('#sid').html(data.id);
                    $('#name').text(data.name);
                    $('#email').text(data.email);
                } else {
                    alert("Student with id = "+ studentId +" cannot be found");
                }
            });
        };
        searchStudent("riad");
    });
</script>
</body>
</html>
