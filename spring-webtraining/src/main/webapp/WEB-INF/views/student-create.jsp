<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ABsiddik
  Date: 2/4/2017
  Time: 2:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create Student</title>
</head>
<body>
<c:if test="${not empty studentList}">
    <table>
        <thead>
        <tr>
            <th>User Name</th>
            <th>Full Name</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${studentList}" var="aStudent">
            <tr>
                <td>${aStudent.userName}</td>
                <td>${aStudent.name}</td>
                <td>${aStudent.email}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>
<h1>Create new Studdent</h1>
<form action="/student/create" method="POST">
    <label for="username">User Name : </label> <input id="username" type="text" name="userName"/>
    <label for="name">Name : </label> <input id="name" type="text" name="name"/>
    <label for="name">Email : </label> <input id="email" type="email" name="email"/>
    <input type="SUBMIT" value="Create" name="submit"/>
</form>

</body>
</html>
